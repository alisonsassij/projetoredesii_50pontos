class vsftpd {

  package { "vsftpd": 
    ensure  => present,
    require => Class["apache"],
  }

  service { "vsftpd":  
    ensure  => "running",
    require => Package["vsftpd"],
  }

}
