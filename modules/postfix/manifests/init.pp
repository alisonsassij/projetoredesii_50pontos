

class postfix {


#Realiza a instalação do pacote postfix, garantindo que o system-update foi realizado
package { 'postfix': 
require => Class["system-update"], # para garantir que o pacote esteja disponivel e atualizado
ensure  => present,
}   


#Inicializa o serviço postfix, após garantir que o arquivo de configuração (main.cf) foi criado com sucesso
service { 'postfix':
ensure  => running,
enable  => true,
hasrestart => true,
hasstatus => true,
require => File['/etc/postfix/main.cf'],
}


#Copia o arquivo main.cf para dentro da pasta do postfix, com as informações de configuração do mesmo
file { '/etc/postfix/main.cf':
source => 'puppet:///modules/arquivos/main.cf',
owner   => 'root',
group   => 'root',
mode    => 644,
require => Package['postfix'],
notify  => Service['postfix'],
}

}
