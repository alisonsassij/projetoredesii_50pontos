class telnet {

  package { "telnetd":   
    ensure  => present,
    require => Class["system-update"],
  }

  service { "openbsd-inetd":    
    ensure  => "running",
    require => Package["telnetd"],
  }

}