class regras{
	file {'/etc/init.d/regras.sh':
	ensure => present,
	owner  => 'root',
	group  => 'root',
	mode   => '0755',
	source => 'puppet:///modules/regras/regras.sh',
	}
}