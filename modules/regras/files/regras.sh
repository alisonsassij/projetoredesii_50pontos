#!/bin/bash

sudo iptables -A INPUT -p icmp --icmp-type echo-request -j DROP
sudo iptables -A INPUT -p tcp --syn --dport 22 -m connlimit --connlimit-above 2 -j REJECT


#sudo iptables -I FORWARD -m string --algo bm --string "skype.com" -j DROP
#sudo iptables -I FORWARD -m string --algo bm --string "web.skype.com" -j DROP
#sudo iptables -A INPUT -p tcp --syn --dport 443 -j REJECT
#sudo iptables -A OUTPUT -p tcp --syn --dport 443 -j REJECT
