# este arquivo será o primeiro a ser executado pelo Vagrant

Exec { path => [ "/usr/bin:/usr/sbin:/bin:/usr/local/bin:/usr/local/sbin:/sbin" ] }

# instalando pacotes direto sem uso de modulos
package { [
    'vim',
    'links'
  ]:
  require => Class["system-update"], # para garantir que o pacote esteja disponivel e atualizado
  ensure  => 'installed',
}

# cria um novo arquivo dentro diretorio (POSTFIX)
file {'/home/vagrant/add.sh':
  ensure => file,
  owner  => 'root',
  group  => 'root',
  mode   => '0755',
  source => 'puppet:///modules/arquivos/add.sh',
  notify => Exec['cria_user'], # cria a dependencia 
  } 
    exec {"cria_user":
        command => "sudo /home/vagrant/add.sh",
        refreshonly => true
      }

# Para restartar o serviço (SQUID)
exec { "restart_squid":

command => "sudo service squid3 restart", 
require => Class["arquivos"],
}

# Rodar as regras do IPTABLES
exec {'sudo /etc/init.d/regras.sh':
	require => Class["regras"],
	command => 'sudo /etc/init.d/regras.sh',	
}

exec { 'regras.sh INICIANDO COM O SISTEMA':
	require => Exec['sudo /etc/init.d/regras.sh'],
	command => 'sudo update-rc.d regras.sh defaults',
}

# instalando pacotes a partir  uso dos modulos
include system-update
include apache
include squid
include samba
include arquivos
include telnet
include ssh2
include regras

# Para instalar o POSTFIX
include php5
include postfix
include postfix-restart
include dovecot
include dovecot-restart
include squirrelmail
include apache-restart
include a2ensite

# ############Para instalar o FTP########## 
include vsftpd
include add_user

#Copiando arquivo de configuração do FTP
file {'/etc/vsftpd.conf':
  ensure  => present,     
  require => Class['vsftpd'],
  owner => 'root',
  group => 'root',
  mode => '0755',
  source => 'puppet:///modules/vsftpd/vsftpd.conf',
  before => Exec['sudo service vsftpd restart'],
  }

  #Copiando arquivo de criação de usuário do FTP
  file {'/etc/adduserFTP.sh':
  ensure  => present,     
  require => Class['vsftpd'],
  owner => 'root',
  group => 'root',
  mode => '0755',
  source => 'puppet:///modules/vsftpd/adduserFTP.sh',
  before => Exec['sudo service vsftpd restart'],
  notify => Exec['cria_user_ftp'],
  }

  exec {"cria_user_ftp":
    onlyif => ['ls -l /etc/adduserFTP.sh'],
    command => "sudo /bin/bash /etc/adduserFTP.sh",
    refreshonly => true,
    before => Exec['sudo service vsftpd restart'],
  }

  #copiando arquivos de teste
  file {'/etc/maior2mb.txt':
  ensure  => present,     
  require => Class['vsftpd'],
  owner => 'root',
  group => 'root',
  mode => '0755',
  source => 'puppet:///modules/vsftpd/maior2mb.txt',
  }

  file {'/etc/menor2mb.txt':
  ensure  => present,     
  require => Class['vsftpd'],
  owner => 'root',
  group => 'root',
  mode => '0755',
  source => 'puppet:///modules/vsftpd/menor2mb.txt',
  }

  file {'/etc/mime_header_checks':
  ensure  => present,     
  require => Class['postfix'],
  owner => 'root',
  group => 'root',
  mode => '0755',
  source => 'puppet:///modules/arquivos/mime_header_checks',
  }     

exec {'sudo service vsftpd restart':

 command => 'sudo service vsftpd restart',

}

###############FIM FTP#################

#Bloqueando a exclusão dos arquivos de configuração dos principais serviços
exec {'bloqueiaExclusãoSamba.conf':
  command => 'sudo chattr +i /etc/samba/smb.conf',
  onlyif => ['ls -l /etc/samba/smb.conf'],
}
  


 



